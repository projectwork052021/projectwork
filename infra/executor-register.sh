#!/bin/bash

# Прочитаем токен для регистрации Gitlab runner. (Находится в разделе Settings->CI/CD->Runners проекта на Gitlab.com)
token=$(<token.env)

dev_ip_addr=$(terraform output -state=terraform/terraform.tfstate -json project_dev | jq -r '.[0]')
ssh-keygen -f ~/.ssh/known_hosts -R $dev_ip_addr
eval 'echo register executor on $dev_ip_addr'
ssh -o "StrictHostKeyChecking no" ubuntu@$dev_ip_addr docker exec gitlab-runner gitlab-runner register --url https://gitlab.com/ --registration-token $token --non-interactive --locked=false --name DockerRunner --executor docker --docker-image alpine:latest --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" --tag-list "linux,xenial,ubuntu,docker" --run-untagged
