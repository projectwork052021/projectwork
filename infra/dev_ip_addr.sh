#!/bin/bash
dev_ip_addr=$(terraform output -state=terraform/terraform.tfstate -json project_dev | jq -r '.[0]')
stage_ip_addr=$(terraform output -state=terraform/terraform.tfstate -json project_stage | jq -r '.[0]')
prod_ip_addr=$(terraform output -state=terraform/terraform.tfstate -json project_prod | jq -r '.[0]')
eval 'echo Project VMs IP address: Dev:$dev_ip_addr Stage:$stage_ip_addr Prod:$prod_ip_addr'

# формируем файл с переменной IP Address, Dev, Stage и Prod  окружения для добавления ее в пайплайн
printf '\055--\nvariables:\n%2sPROJECT_DEV_IP_ADDR:%1s'$dev_ip_addr'\n%2sPROJECT_STAGE_IP_ADDR:%1s'$stage_ip_addr'\n%2sPROJECT_PROD_IP_ADDR:%1s'$prod_ip_addr'\n' > ../.var.yml
