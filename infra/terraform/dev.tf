resource "yandex_compute_instance" "dev" {
  name                      = "project-dev-${count.index}"
  count                     = var.count_of_instances
  allow_stopping_for_update = true
  platform_id               = var.platform

  resources {
    cores         = var.cores
    memory        = var.memory
    core_fraction = var.core_fraction
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
      size     = var.disk_size
    }
  }

  network_interface {
    # Указан id подсети default-ru-central1-a
    subnet_id = var.subnet_id
    nat       = var.is_nat
  }

  metadata = {
    ssh-keys = "ubuntu:${file(var.public_key_path)}"
  }
}
