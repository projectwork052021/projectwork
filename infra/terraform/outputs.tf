output "project_dev" {
  value = yandex_compute_instance.dev.*.network_interface.0.nat_ip_address
}
output "project_stage" {
  value = yandex_compute_instance.stage.*.network_interface.0.nat_ip_address
}
output "project_prod" {
  value = yandex_compute_instance.prod.*.network_interface.0.nat_ip_address
}
