---
image: python:3.6.15-alpine3.14

include:
  local: .var.yml

stages:
  - test
  - build
  - review
  - stage
  - production

variables:
  PROJECT_NAME: projectwork
  UI_NAME: 'project_ui'
  CRAWLER_NAME: 'project_crawler'

build_job:
  stage: build
  image: docker:stable
  variables:
    IMAGE_UI: $DOCKER_REGISTRY_LOGIN/$UI_NAME:1.0
    IMAGE_CRAWLER: $DOCKER_REGISTRY_LOGIN/$CRAWLER_NAME:1.0
  script:
    - echo 'Clone repo'
    # Установим git
    - apk add git

    # Для RO-доступа используем клонирование с помощью https
    - cd project-app && git clone https://github.com/express42/search_engine_ui.git && git clone https://github.com/express42/search_engine_crawler.git

    - echo 'Building'
    - docker info
    - docker login -u $DOCKER_REGISTRY_LOGIN -p $DOCKER_REGISTRY_PASS
    - docker build -f Dockerfile_ui -t $IMAGE_UI ./search_engine_ui
    - docker image push $IMAGE_UI
    - docker build -f Dockerfile_crawler -t $IMAGE_CRAWLER ./search_engine_crawler
    - docker image push $IMAGE_CRAWLER
  only:
    - branches
  except:
    - main
  tags:
    - docker

# include:
#    - template: Security/Container-Scanning.gitlab-ci.yml

deploy_review:
  stage: review
  image: tmaier/docker-compose:latest
  script:
    - echo "Deploy branch $CI_COMMIT_REF_NAME"
    - cd project-app
    - docker-compose rm -sf $UI_NAME
    - docker-compose rm -sf $CRAWLER_NAME
    - docker-compose up -d $UI_NAME
    - docker-compose up -d $CRAWLER_NAME
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: http://$PROJECT_DEV_IP_ADDR:8000
  only:
    - branches
  except:
    - main

test_unit_job_ui:
  stage: test
  services:
    - name: mongo:5.0.3
  script:
    - apk add git
    - cd project-app && git clone https://github.com/express42/search_engine_ui.git
    - cd search_engine_ui
    - pip install -r requirements.txt -r requirements-test.txt
    - python -m unittest discover -s tests/
  only:
    - branches
  except:
    - main

test_coverage_job_ui:
  stage: test
  services:
    - name: mongo:5.0.3
  script:
    - apk add git
    - cd project-app && git clone https://github.com/express42/search_engine_ui.git
    - cd search_engine_ui
    - pip install -r requirements.txt -r requirements-test.txt
    - coverage run -m unittest discover -s tests/
    - coverage report --include ui/ui.py
  only:
    - branches
  except:
    - main

test_unit_job_crawler:
  stage: test
  services:
    - name: mongo:5.0.3
    - name: rabbitmq:3.8-management
  script:
    - apk add git
    - cd project-app && git clone https://github.com/express42/search_engine_crawler.git
    - cd search_engine_crawler
    - pip install -r requirements.txt -r requirements-test.txt
    - python -m unittest discover -s tests/
  only:
    - branches
  except:
    - main

test_coverage_job_crawler:
  stage: test
  services:
    - name: mongo:5.0.3
    - name: rabbitmq:3.8-management
  script:
    - apk add git
    - cd project-app && git clone https://github.com/express42/search_engine_crawler.git
    - cd search_engine_crawler
    - pip install -r requirements.txt -r requirements-test.txt
    - coverage run -m unittest discover -s tests/
    - coverage report --include crawler/crawler.py
  only:
    - branches
  except:
    - main

deploy_stage:
  stage: stage
  image: docker:stable
  script:
   - echo 'Deploy to Stage'
   # добавим host в список known hosts контейнера
   - ssh -o StrictHostKeyChecking=no $SSH_LOGIN@$PROJECT_STAGE_IP_ADDR
   # перейдем в локальный репозиторий в контейнере
   - cd /builds/projectwork052021/projectwork/project-app
   # скопируем docker-compose.yml и .env приложения на Stage
   - scp /builds/projectwork052021/projectwork/project-app/docker-compose.yml $SSH_LOGIN@$PROJECT_STAGE_IP_ADDR:~/
   - scp /builds/projectwork052021/projectwork/project-app/.env $SSH_LOGIN@$PROJECT_STAGE_IP_ADDR:~/
   - ssh $SSH_LOGIN@$PROJECT_STAGE_IP_ADDR "docker-compose rm -sf $UI_NAME"
   - ssh $SSH_LOGIN@$PROJECT_STAGE_IP_ADDR "docker-compose rm -sf $CRAWLER_NAME"
   - ssh $SSH_LOGIN@$PROJECT_STAGE_IP_ADDR "docker-compose up -d $UI_NAME"
   - ssh $SSH_LOGIN@$PROJECT_STAGE_IP_ADDR "docker-compose up -d $CRAWLER_NAME"
  environment:
    name: staging
    url: http://$PROJECT_STAGE_IP_ADDR:8000
  # only:
  #   - merge_request
  rules:
    #- if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main"'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy_prod:
  stage: production
  image: docker:stable
  script:
   - echo 'Deploy to Prod'
   # добавим host в список known hosts контейнера
   - ssh -o StrictHostKeyChecking=no $SSH_LOGIN@$PROJECT_PROD_IP_ADDR
   # перейдем в локальный репозиторий в контейнере
   - cd /builds/projectwork052021/projectwork/project-app
   # скопируем docker-compose.yml и .env приложения на Prod
   - scp /builds/projectwork052021/projectwork/project-app/docker-compose.yml $SSH_LOGIN@$PROJECT_PROD_IP_ADDR:~/
   - scp /builds/projectwork052021/projectwork/project-app/.env $SSH_LOGIN@$PROJECT_PROD_IP_ADDR:~/
   - ssh $SSH_LOGIN@$PROJECT_PROD_IP_ADDR "docker-compose rm -sf $UI_NAME"
   - ssh $SSH_LOGIN@$PROJECT_PROD_IP_ADDR "docker-compose rm -sf $CRAWLER_NAME"
   - ssh $SSH_LOGIN@$PROJECT_PROD_IP_ADDR "docker-compose up -d $UI_NAME"
   - ssh $SSH_LOGIN@$PROJECT_PROD_IP_ADDR "docker-compose up -d $CRAWLER_NAME"
  environment:
    name: production
    url: http://$PROJECT_PROD_IP_ADDR:8000
  when: manual
  only:
   - /^\d+\.\d+\.\d+/

before_script:
  ##
  ## Install ssh-agent if not already installed, it is required by Docker.
  ## (change apt-get to yum if you use an RPM-based image)
  ##
  - 'command -v ssh-agent >/dev/null || (apk add openssh-client)'

  ##
  ## Run ssh-agent (inside the build environment)
  ##
  - eval $(ssh-agent -s)

  ##
  ## Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
  ## We're using tr to fix line endings which makes ed25519 keys work
  ## without extra base64 encoding.
  ## https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
  ##
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -

  ##
  ## Create the SSH directory and give it the right permissions
  ##
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh

  ##
  ## Optionally, if you will be using any Git commands, set the user name and
  ## and email.
  ##
  # - git config --global user.email "user@example.com"
  # - git config --global user.name "User name"
