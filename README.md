# Проектная работа
Тема проектной работы - Создание процесса непрерывной поставки для приложения с применением Практик CI/CD и быстрой обратной связью

**Группа Otus-DevOps-2021-05**
Курс обучения [DevOps практики и инструменты](https://otus.ru/lessons/devops-praktiki-i-instrumenty/)

## Требование к проекту

1. Автоматизированные процессы создания и управления платформой
    - Ресурсы Ya.cloud
    - Инфраструктура для CI/CD
    - Инфраструктура для сбора обратной связи
2. Использование практики IaC (Infrastructure as Code) для управления конфигурацией и инфраструктурой
3. Настроен процесс CI/CD
4. Все, что имеет отношение к проекту хранится в Git
5. Настроен процесс сбора обратной связи
    - Мониторинг (сбор метрик, алертинг, визуализация)
    - Логирование (опционально)
    - Трейсинг (опционально)
    - ChatOps (опционально)
6. Документация
    - README по работе с репозиторием
    - Описание приложения и его архитектуры
    - How to start?
    - ScreenCast
    - CHANGELOG с описанием выполненной работы
    - Если работа в группе, то пункт включает автора изменений

## Работа с репозиторием Gitlab.com

Для выполнения проектной работы нам подойдет репозиторий предоставляемый как сервис Gitlab.com.
Для использования репозитория Gitlab.com и CI/CD пайплайна необходимо зарегистрироваться, например, с учетной записью GitHub.
Создадим Группу **projectwork052021** и проект **projectwork**.
![image 1](images/gitlabproject.png)
В настройках проекта CI/CD Settings>Runners получить registration token для подключения (регистрации) внешнего Runners, в нашем случае
мы используем docker executors. Сохраним токен в файле `infra/token.env`
В разделе CI/CD Settings>Variables добавим используемые нами переменные в пайплайне, например, Имя пользователя и пароль в Docker Registry. SSH_LOGIN и SSH_PRIVATE_KEY используемые в процессе работы Docker executor для деплоя на VM Prod и Stage.
В настройках профиля необходимо добавить публичный SSH ключ машины разработчика.

## Описание приложения и его архитектура

У нас есть микросервисное приложение представляющее собой  Поисковый бот (**Crawler**) и Веб-интерфейс (**UI**) поиска слов и фраз на проиндексированных ботом сайтах. Также используются дополнительные сервисы: **MongoDb** для хранения проиндексированной информации, **RabbitMQ** в качестве менеджера очереди сообщений, **Prometheus**, **mongodb-exporter**, **node-exporter** для сбора метрик с инфраструктуры и микросервисов. Для логирования работы микросервисов используется стек **EFK** (Elasticsearch + Fluentd + Kibana).
В качестве вводной информации для парсинга используется сайт `https://vitkhab.github.io/search_engine_test_site/`

Принцип работы приложения:
1. Бот **Crawler** помещает в очередь `url` переданный ему при запуске. Затем он начинает обрабатывать все `url` в очереди. Для каждого `url` бот загружает содержимое страницы, записывая в БД связи между сайтами, между сайтами и словами. Все найденные на странице `url` помещает обратно в очередь.
2. Для поиска слов и фраз на проиндексированных ботом **Crawler** сайтах используется веб интерфейс **UI**. Веб-интерфейс минимален, предоставляет пользователю строку для запроса и результаты. Поиск происходит только по индексированным сайтам. Результат содержит только те страницы, на которых были найдены все слова из запроса. Рядом с каждой записью результата отображается оценка полезности ссылки (чем больше, тем лучше).
![image 2](images/ui.png)

## Описание приложения и его архитектура

У нас есть микросервисное приложение представляющее собой  Поисковый бот (**Crawler**) и Веб-интерфейс (**UI**) поиска слов и фраз на проиндексированных ботом сайтах. Также используются дополнительные сервисы: **MongoDb** для хранения проиндексированной информации, **RabbitMQ** в качестве менеджера очереди сообщений, **Prometheus**, **mongodb-exporter**, **node-exporter** для сбора метрик с инфраструктуры и микросервисов.
В качестве вводной информации для парсинга используется сайт `https://vitkhab.github.io/search_engine_test_site/`

Принцип работы приложения:
1. Бот **Crawler** помещает в очередь `url` переданный ему при запуске. Затем он начинает обрабатывать все `url` в очереди. Для каждого `url` бот загружает содержимое страницы, записывая в БД связи между сайтами, между сайтами и словами. Все найденные на странице `url` помещает обратно в очередь.
2. Для поиска слов и фраз на проиндексированных ботом **Crawler** сайтах используется веб интерфейс **UI**. Веб-интерфейс минимален, предоставляет пользователю строку для запроса и результаты. Поиск происходит только по индексированным сайтам. Результат содержит только те страницы, на которых были найдены все слова из запроса. Рядом с каждой записью результата отображается оценка полезности ссылки (чем больше, тем лучше).

## Необходимое программное обеспечение на машине разработчика

Для разработки проекта используется VM под управлением OS Ubuntu 20.04 x64, среда разработки VS Code.

Перед началом разработки или развертывания проекта на рабочей машине разработчика необходимо установить следующее ПО:

- Yandex Cli, для создания и использования VM мы пользуемся услугами Yandex Cloud, для взаимодействия с облаком используется CLI, инструкция по установке `https://cloud.yandex.ru/docs/cli/operations/install-cli` Для развертывания проекта можно использовать любое другое облако, со схожим функционалом и поддержкой Packer, Terraform.
- HashiCorp Packer v1.7.6, установку производим согласно инструкции `https://learn.hashicorp.com/tutorials/packer/get-started-install-cli`
- HashiCorp Terraform v1.0.8, установку производим согласно инструкции  `https://www.terraform.io/downloads.html`
- Docker v20.10.9, для локальной отладки, установка согласно инструкции  `https://docs.docker.com/engine/install/ubuntu/`
- Docker-Compose v1.25.0, для локальной отладки, установка согласно инструкции `https://docs.docker.com/compose/install/`
- Ansible v2.11.4, установка согласно инструкции `https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html`
- Ansible geerlingguy.docker - `https://github.com/geerlingguy/ansible-role-docker`
- Ansible Collections Community.Docker - `https://docs.ansible.com/ansible/latest/collections/community/docker/index.html`
- jq - commandline JSON processor [version 1.6], установка ```apt install jq```
- git  последней версии, установка `apt install git`

## Развертывание VM проекта

1. Для развертывания проекта с машины разработчика, необходимо выполнить команду `git clone git@gitlab.com:projectwork052021/projectwork.git` и перейти в созданный каталог `projectwork`.

Проект состоит из следующей структуры:
```
darkon@darkonVM:~/projectwork (main)$ tree
.
├── CHANGELOG.md
├── docker
│   ├── docker-compose-logging.yml
│   └── docker-compose.yml
├── DOING.md
├── images
│   ├── gitlab_env.png
│   ├── gitlab_merge.png
│   ├── gitlab_pipeline.png
│   ├── gitlabproject.png
│   ├── graphana_project_app.png
│   ├── kibana01.png
│   ├── kibana03.png
│   ├── kibana04_ui.png
│   ├── prometheus_graph_down.png
│   ├── prometheus_graph_up.png
│   ├── prometheus_targets.png
│   ├── rabbitmq1.png
│   ├── rabbit_ui.png
│   └── ui.png
├── infra
│   ├── ansible
│   │   ├── ansible.cfg
│   │   ├── dynamic_inventory_json.py
│   │   ├── playbooks
│   │   │   ├── docker_preinstall.yml
│   │   │   └── packer_project-host.yml
│   │   └── requirements.yml
│   ├── dev_ip_addr.sh
│   ├── executor-register.sh
│   ├── image_id.sh
│   ├── packer
│   │   ├── project.json
│   │   └── variables.json.example
│   ├── terraform
│   │   ├── dev.tf
│   │   ├── key_tf.json.example
│   │   ├── main.tf
│   │   ├── outputs.tf
│   │   ├── prod.tf
│   │   ├── stage.tf
│   │   ├── terraform.tfvars.example
│   │   └── variables.tf
│   └── token.env.example
├── LICENSE
├── logging
│   └── fluentd
│       ├── Dockerfile
│       └── fluent.conf
├── Makefile
├── monitoring
│   ├── grafana
│   │   ├── dashbords.example.json
│   │   └── rabbitMQ_dashboard.json
│   └── prometheus
│       ├── Dockerfile
│       └── prometheus.yml
├── project-app
│   ├── docker-compose.yml
│   ├── Dockerfile_crawler
│   └── Dockerfile_ui
└── README.md

13 directories, 50 files
```

2. Для автоматизации выполняемых действий при развертывании проекта используется команда `make` и `Makefile` размещенный в корне проекта. Выполнив команду `make` будет отображены доступные действия автоматизации:
```bash
darkon@darkonVM:~/projectwork (main)$ make
prepare_VMs           Create    Image,   VMs         and        preinstall  services
all_for_VM_wo_packer  VMs       and      preinstall  services
packer_build          Build     image    disk        by         Packer      with      Docker
terraform_automation  Create    VMs      by          terraform
destroy_VMs           Destroy   VMs      by          terraform
ansible_service       ansible   service  preinstall
register_runner       Register  Docker   executor
show_project_IP       Show      project  VMs         IP         Address
```
Соответственно, выполняя команду **make packer_build** будет произведено развертывание в облаке VM, провиженинг при помощи плейбука `infra/ansible/playbooks/packer_project-host.yml` необходимого ПО и создание образа для дальнейшего развертывания проекта. Id образа сохраняется при помощи механизма post-processors и формируется файл `infra/terraform/image_id.auto.tfvars`, используемый далее в Terraform при создании VM.

Команда **make terraform_automation** создает VM проекта (dev, stage и prod) в облаке, с заданной в манифестах terraform конфигурацией, указанной в `infra/terraform/variables.tf`. Для взаимодействия Terraform c облаком необходимо настроить сервисный аккаунт и подготовить `infra/terraform/key_tf.json` (пример содержимого файла  `infra/terraform/key_tf.json.example`), а также другие значения переменных в файле `infra/terraform/terraform.tfvars` (пример содержимого файла `infra/terraform/terraform.tfvars.example`).

Команда **make ansible_service** развертывает на предустановленные VM, с помощью плейбука `infra/ansible/playbooks/docker_preinstall.yml`, все необходимые сервисы для проекта, а также устанавливает `gitlab-runner`, на котором в дальнейшем будет выполняться пайплайн. В качестве инвентори в Ansible используется динамическое инвентори `infra/ansible/dynamic_inventory_json.py`, которое в свою очередь, использует в качестве входных данных вывод команды `terraform state pull`. Рабочий каталог terraform проекта, передается через переменную окружения `TF_STATE` или через файл `infra/ansible/env_tf_state.env`. В проекте используется по умолчанию файл, он формируется автоматически при выполнении команды **make terraform_automation**

Команда **make show_project_IP** используется для отображения IP адресов VM проекта, а также сохранение их в файл `.var.yml`, для включения этих переменных в файл пайплайна.

Для полной подготовки инфраструктуры используется команда **make prepare_VMs**, результатом которой будет полностью подготовлен образ VM в облаке, затем развернуты VM (dev, stage и prod) и установлены сервисные службы в Docker-контейнерах. А также автоматически зарегистрирован Docker executor на dev среде.
Команда **make all_for_VM_wo_packer** аналогична описанной команде **make prepare_VMs**, за исключением стадии **make packer_build**.

Команда **make destroy_VM** используется для удаления VM в облаке с помощью Terraform.

3. Регистрация gitlab-runner

Команда **make register_runner** регистрирует Docker executor проекта в сервисе Gitlab.com

ScreenCast развертывания инфраструктуры доступен по ссылке [Infra prepare ScreenCast](https://youtu.be/nEx2kSJwds0)

### Детальное описание развертывания инфраструктуры

В предыдущем пункте `## Развертывание VM проекта` было описано, как развернуть проект при помощи скриптов автоматизации, сейчас мы покажем, как устроена инфраструктура в проекте.

В проекте предполагается наличие трех окружений:
- dev - для разворачивания приложений при любом коммите в любой ветке кроме `main`
- stage - для разворачивания приложений при мерже в ветку `main`
- prod - для разворачивания приложений при установке тега на коммит ветки `main`

Для этого подготовим инфраструктуру в виде 3 VM для каждого окружения в облаке Yandex Cloud.

#### Подготовка образа диска виртуальной машины с помощью Packer и Ansible

Для автоматизации развертывания каждой виртуальной машины подготовим конфигурационные файлы Packer и Terraform, а также напишем плейбуки Ansible.
Инвентори будем генерить динамическое на основе вывода команды terraform state pull.

1. Подготовим образ с предустановленным Docker, Docker-Compose и python3, они нам будут нужны для хостинга Docker контейнеров с приложениями и вспомогательными сервисами, а также для удаленной установки, провижионинг же осуществим при помощи плейбука `infra/ansible/playbooks/packer_project-host.yml`.

Запуск создания образа выполняем из каталога infra/
Сам образ хранится в облаке Yandex Cloud.

```
packer build -machine-readable -var-file=packer/variables.json packer/project.json
```

*пример содержания файла variables.json представлен в виде файла- variables.json.example*

2. Полученный Id образа внесем в файл переменных *infra/terraform/terraform.tfvars*, пример содержания файла - *infra/terraform/terraform.tfvars.example*. Либо создадим файл `image_id.auto.tfvars` c id образа автоматически используя пост-процессор в Packer.

#### Создание хостов в Yandex Cloud при помощи Terraform

Подготовленная *Infrastructure as code* (IasC) располагается в каталоге infra/terraform проекта.

Выполним **terraform init**  для и инициализации и **terraform plan и terraform apply** для развертывания виртуальных машин хостинга Docker контейнеров.
Команды выполняем из каталога infra/terraform.  Output переменная после создания VM  будет использоваться в динамическом инвентори.

```shell
terraform apply
```

#### Провижининг при помощи Ansible

1. Для работы динамического инвентори `infra/ansible/dynamic_inventory_json.py`, в конфигурационный файл *env_tf_state.env* внесем абсолютный путь до каталога с конфигурацией terraform:
```
/home/darkon/project_work/infra/terraform
```
2. Предустановка сервисных приложений, таких как MongoDB, RabbitMQ, Gitlab-runner, Prometheus, стек EFK выполняется при помощи запуска плейбука infra/ansible/playbooks/docker_preinstall.yml

## Запуск проекта на dev окружении

Для развертывания проекта на dev окружении достаточно выполнить команду создания новой ветки, например,
```bash
git checkout-b first_start
```
и сделать commit && push
```
git commit -am "first_commit" && git push
```
После этого запустится обработка проекта в пайплайне, описанном в файле ```.gitlab-ci.yml```

После успешного выполнения всех стадий пайплайна, на [странице проекта](https://gitlab.com/projectwork052021/projectwork/-/environments), появится окружение dev и будет доступна ссылка для доступа к веб **UI** нашего приложения.
![image 3](images/gitlab_env.png)

Кроме того, по тому-же IP-адресу, на портах:
 - :9090  - web ui службы Prometheus для сбора метрик
 - :3000  - web ui службы Grafana для отображения метрик (логин/пароль admin/admin123)
 - :15672 - web ui службы rabbutMQ
 - :5601  - web ui службы Kibana для отображения собранных логов

Для проекта настроен ChatOps в канал SLACK `https://devops-team-otus.slack.com/archives/C02GJ6N1ABX`
![image 4](images/slack.png)

## Процесс сбора обратной связи
### Мониторинг (сбор метрик, алертинг, визуализация)

Для осуществления мониторинга в проект включены вспомогательные сервисы, такие как:
- Prometheus работающий в Docker контейнере. Конфигурационный файл `monitoring/prometheus/prometheus.yml` и Dockerfile для сборки образа Prometheus находятся  в `monitoring/prometheus`.
В Prometheus настроен сбор метрик с mongodb при помощи bitnami-mongodb-exporter, с VM при помощи node-exporter, из rabbitMQ при помощи стандартного интерфейса, а также с приложений проекта **ui** и **crawler**.
![image 5](images/prometheus_graph_down.png)
Контроль доступности Endpoint'ов осуществляется в Prometheus по адресу `http://ip_адрес_окружения_проекта:9090` в разделе *Status->Targets*.
![image 6](images/prometheus_targets.png)
При необходимости, просмотреть отдельные метрики можно в самом Prometheus в разделе *Graph* и далее выбрав требуемую метрику в комбобоксе, но для большего удобства лучше использовать Grafana.

- Grafana работающая в Docker контейнере, предназначена для отображения метрик в удобном для пользователя виде. Настройка дашбордов в Grafana осуществляется  следующим образом. После авторизации в web-интерфейсе Grafana по адресу `http://ip_окружение_проекта:3000`, необходимо в разделе *Configurations->Data sources->Add data source* выбрать в качестве источника данных Prometheus, в поле `URL` задать `http://prometheus:9090` и нажать *Save & test*. Далее в разделе *Create-> Dashboard->add an empty panel* выбрать необходимые для наблюдения метрики. Для каждого окружения может быть свой набор метрик.
Пример настройки Dashboard в виде json представлен в файле `monitoring/grafana/dasbords.example.json`
![image 7](images/graphana_project_app.png)
Пример отображения метрик с rabbitMQ ![image 8](images/rabbitmq1.png)

Установка сервисов производится на этапе подготовки каждого VM окружения (dev, stage и prod) с использованием ansible плейбука `infra/ansible/playbooks/docker_preinstall.yml` и манифеста `docker/docker-compose.yml`

### Логирование (опционально)

Для осуществления сбора логов используется стек EFK (Elasticsearch + Fluentd + Kibana).

Docker контейнер Fluentd собирается на основе `logging/fluentd/Dockerfile` и конфигурационного файла `logging/fluentd/fluent.conf`.
Collector Fluentd настроен на сбор логов от описанных в конфигурационном файле источников, на порту 24224. В свою очередь, в docker-compose.yml файл сервисов, с которых предполагается собирать логи необходимо, добавить код с драйвером fluentd, адресом куда отправлять логи и указать tag лога, для дальнейшей работы.
```
logging:
      driver: "fluentd"
      options:
        fluentd-address: localhost:24224
        tag: service.ui
```
После подключения драйвера fluentd в контейнер приложения, оно становится зависимым от наличия запущенного контейнера с fluent и если он не принимает логи, т.е. не запущен, то и эти контейнеры не запускаются.

Мы настроим сбор логов с приложений **ui**,  **crawler**,  **mongodb** и **rabbitmq**.
После того, как Fluentd принимает логи от приложений, их можно распарсить json парсером или регулярным выражением и сохранить их в Elasticsearch.
Отображение логов осуществляется при помощи web ui Kibana. Для настройки последней, необходимо в разделе Discover, создать Index Pattern, указать имя паттерна `fluentd-*` и на втором шаге выбрать Time Filter field name `@timestamp`, далее нажать Create index pattern.
После этого будет доступна работа с накопленными логами.
![image 9](images/kibana01.png)
![image 10](images/kibana03.png)
![image 11](images/kibana04_ui.png)

## CI/CD Pipeline

В CI/CD описаны стадии:
- test
- build
- review
- stage
- production

![image 12](images/gitlab_pipeline_vis.png)
В рамках стадий, может быть одна и более задач.

Все задачи в рамках стадий выполняются в docker контейнерах, что позволяет исключить внешнее влияние на процесс описанный в каждой задаче.

Принцип заложенный в CI/CD проектной работы:
1. при коммите в любую ветку кроме main выполняются стадии:
   - test - unit и coverage тесты приложения в контейнера
   - build - сборка docker образов приложений и отправка их в Docker HUB ![image 13](images/DockerHub.png)
   - review - выкатка приложения на VM dev
2. при мерже в ветку main выполняется стадия:
   - stage - выкатка приложения на VM stage
3. при установке цифрового тега версии (x.x.x) на коммит ветки main выполняется стадия:
   - production в рамках которой, появляется возможность вручную запустить выкатку на VM prod
![image 14](images/gitlab_pipeline.png)

Для того чтобы из контейнера, в котором Executor выполняет CI/CD стадий stage и production можно было достучаться до VM Stage и Prod, добавлена стадия before_script, необходимая для передачи приватного ssh ключа в контейнер.
