.DEFAULT_GOAL := help
.PHONY: help

PROJ_ROOT=$(PWD)

help:
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) \
	| sed -n 's/^\(.*\): \(.*\)##\(.*\)/\1\3/p' \
	| column -t  -s ' '

prepare_VMs: packer_build terraform_automation ansible_service register_runner show_project_IP  ## Create Image, VMs and preinstall services

all_for_VM_wo_packer: terraform_automation ansible_service register_runner show_project_IP  ## VMs and preinstall services

packer_build: ## Build image disk by Packer with Docker
	@cd infra && packer build -machine-readable -var-file=packer/variables.json packer/project.json

terraform_automation: ## Create VMs by terraform
	@cd infra/terraform && terraform init -input=false && terraform plan -out=tfplan -input=false \
	&& terraform apply -input=false tfplan
	echo $(PROJ_ROOT)/infra/terraform > $(PROJ_ROOT)/infra/ansible/env_tf_state.env \
	&& sleep 60

destroy_VMs: ## Destroy VMs by terraform
	@cd infra/terraform && terraform init -input=false && terraform plan -out=tfplan -input=false && terraform destroy

ansible_service: ## ansible service preinstall
	@cd infra/ansible && ansible-playbook playbooks/docker_preinstall.yml

register_runner: ## Register Docker executor
	@cd infra/ && ./executor-register.sh

show_project_IP: ## Show project VMs IP Address
	@cd infra/ && ./dev_ip_addr.sh
